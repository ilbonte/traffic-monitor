const request = require("request")
var express = require('express');
var port = process.env.PORT || 8080;

var app = express();

let baseURL = `http://ipchannels.integreen-life.bz.it/origin-destination/rest`;


app.get('/api/getStationDetails', function (req, res) {
    getStationDetails().then(stations => {
        res.send(stations)
    });
});


app.get('/api/getTrafficData', function (req, res) {
    res.json(JSON.parse(trafficDataResponse))
});

app.get('/api/getParkingData', function (req, res) {
    res.json(JSON.parse(parkingDataResponse))
});

app.get('/api/getLastRecordStations', function (req, res) {
    getStationDetails().then(stations => {
        var promises = [];
        JSON.parse(stations).forEach(station => {
            promises.push(getDateOfLastRecord(station.id))
        });

        Promise.all(promises).then(values => {
            var dates = [];
            values.forEach(value => {
                dates.push({
                    station: value.station,
                    date: new Date(Number(value.date))
                })
            });
            res.send(dates)
        })
    });
});

function getStationDetails() {
    return new Promise(resolve => {
        request(`${baseURL}/get-available-stations`, function (error, response, body) {
            resolve(body)
        });
    });
}

function getStationResult(stationID, pastSeconds) {
    return new Promise(resolve => {
        request(`${baseURL}/get-records?station=${stationID}&name=value&seconds=${pastSeconds}`, function (error, response, body) {
            resolve(body)
        });
    });
}

function getDateOfLastRecord(stationID) {
    return new Promise(resolve => {
        request(`${baseURL}/get-date-of-last-record?station=${stationID}`, function (error, response, body) {
            resolve({
                station: stationID,
                date: body
            })
        });
    });
}

//mocked traffic data retrived using openhub's api here: https://gitlab.com/ilbonte/traffic-monitor/blob/master/data-analysis.ipynb
const trafficDataResponse = "{\"06:37:30\":46.5192574629,\"06:52:30\":42.7894847826,\"07:07:30\":39.3584374548,\"07:22:30\":32.4236473938,\"07:37:30\":26.8098351231,\"07:52:30\":27.1168669054,\"08:07:30\":24.4641443035,\"08:22:30\":23.3487712872,\"08:37:30\":27.3842473804,\"08:52:30\":28.9407165955,\"09:07:30\":29.2700800045,\"09:22:30\":28.6691968593,\"09:37:30\":30.3308401183,\"09:52:30\":30.4826778635,\"10:07:30\":28.3540468376,\"10:22:30\":28.1622718027,\"10:37:30\":30.1563230311,\"10:52:30\":31.103372875,\"11:07:30\":29.6513513899,\"11:22:30\":30.1571270863,\"11:37:30\":30.5532441867,\"11:52:30\":30.5407297343,\"12:07:30\":24.0398967039,\"12:22:30\":30.2290630317,\"12:37:30\":30.7464158659,\"12:52:30\":33.2112371223,\"13:07:30\":31.2836825436,\"13:22:30\":32.8021233263,\"13:37:30\":33.2216421492,\"13:52:30\":31.2093523511,\"14:07:30\":31.0504214756,\"14:22:30\":30.338012289,\"14:37:30\":31.9934062276,\"14:52:30\":31.8125564404,\"15:07:30\":30.0128819137,\"15:22:30\":29.8688723164,\"15:37:30\":31.2324503003,\"15:52:30\":32.3244990355,\"16:07:30\":30.3215556699,\"16:22:30\":28.7665734679,\"16:37:30\":28.4166999777,\"16:52:30\":28.3915725266,\"17:07:30\":22.1223481177,\"17:22:30\":22.9848777428,\"17:37:30\":25.7197293353,\"17:52:30\":29.0646088395,\"18:07:30\":27.1071124201,\"18:22:30\":31.2778518174,\"18:37:30\":35.1807206044,\"18:52:30\":37.5672579451,\"19:07:30\":37.8945979041,\"19:22:30\":39.7904430341,\"19:37:30\":41.9024910956}";
const parkingDataResponse = "{\"06:37:30\":0,\"06:52:30\":0,\"07:07:30\":1,\"07:22:30\":1,\"07:37:30\":3,\"07:52:30\":5,\"08:07:30\":11,\"08:22:30\":15,\"08:37:30\":33,\"08:52:30\":27,\"09:07:30\":17,\"09:22:30\":7,\"09:37:30\":3,\"09:52:30\":1,\"10:07:30\":3,\"10:22:30\":1,\"10:37:30\":2,\"10:52:30\":1,\"11:07:30\":3,\"11:22:30\":3,\"11:37:30\":4,\"11:52:30\":5,\"12:07:30\":14,\"12:22:30\":27,\"12:37:30\":23,\"12:52:30\":16,\"13:07:30\":11,\"13:22:30\":14,\"13:37:30\":11,\"13:52:30\":9,\"14:07:30\":3,\"14:22:30\":1,\"14:37:30\":0,\"14:52:30\":0,\"15:07:30\":0,\"15:22:30\":1,\"15:37:30\":0,\"15:52:30\":2,\"16:07:30\":1,\"16:22:30\":3,\"16:37:30\":1,\"16:52:30\":2,\"17:07:30\":2,\"17:22:30\":5,\"17:37:30\":9,\"17:52:30\":14,\"18:07:30\":27,\"18:22:30\":15,\"18:37:30\":13,\"18:52:30\":7,\"19:07:30\":4,\"19:22:30\":3,\"19:37:30\":1}";


app.listen(port, function () {
    console.log('Server started on port ' + port);
});


app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
