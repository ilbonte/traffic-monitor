#### What is this? ⭐️

The purpose of this server is to gather, elaborate and send the data back to the client
in a way he can parse easly. An example is the dashboard we implemented: http://gitlab.com/dallagi/wideopen-frontend

### How it works 🔧

Traffic data are provided by opendatahub: (http://ipchannels.integreen-life.bz.it/LinkFrontEnd/swagger-ui.html#/) 
and the data about companies's parking lot from our prototype IoT deviced connected to the LoRa network.The code is
available here: https://gitlab.com/ilbonte/lora-bluetooth-car-tracker

Note:
Since data for the last days are not avilable in the dataset (yet) we decided to embed them directly in
the code to show our working prototype in the demo. This "mock data" are actually real data calculated
using python. If you are courious about we obtain the data take a look here: https://gitlab.com/ilbonte/traffic-monitor/blob/master/data-analysis.ipynb
Once the sensor will be up and running the data will be fresh and up to date! (We are quite exited about this! :D)

### Wanna help us? 🙏

The data are served using  [expressjs](https://github.com/expressjs/express) so you will need the lasted NodeJS LTS


Install deps: `npm install`


To start the dev server `npm start`

Open `index.js` with your favourite editor and from here it's only 'ol plain JavaScript