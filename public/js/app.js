var map;

function onPageLoaded() {
    loadMap();
    showStations()
}

function showStations() {
    sendRequest("api/getStationDetails").then(stations => {
        stations.forEach(station => {
           showStreets(station)
        })
    });
}

function showStreets(station){
    addLines(station.coordinates, getRandomColor())
}

function addLines(coordinates, color){
    var points = [];

    var epsg4326 =  new OpenLayers.Projection("EPSG:4326");
    var projectTo = map.getProjectionObject();

    coordinates.forEach(coordinate => {
        points.push(new OpenLayers.Geometry.Point( coordinate.lon, coordinate.lat ).transform(epsg4326, projectTo))
    });

    var vectorLayer = new OpenLayers.Layer.Vector("Overlay");

    var feature = new OpenLayers.Feature.Vector(
        new OpenLayers.Geometry.LineString(points)
    );

    var style = OpenLayers.Util.applyDefaults(style, OpenLayers.Feature.Vector.style['default']);
    style.strokeColor = color;
    style.strokeWidth = 6;
    feature.style = style;


    vectorLayer.addFeatures(feature);

    map.addLayer(vectorLayer);


}

function sendRequest(url) {
    return new Promise(resolve => {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                console.log(xhttp.response);
                resolve(JSON.parse(xhttp.response))
            }
        };
        xhttp.open("GET", url, true);
        xhttp.send();
    });
}

function loadMap() {
    map = new OpenLayers.Map("map");
    map.addLayer(new OpenLayers.Layer.OSM());

    var lonLat = new OpenLayers.LonLat(11.3498, 46.4892)
        .transform(
            new OpenLayers.Projection("EPSG:4326"),
            map.getProjectionObject()
        );

    var zoom = 13;

    map.setCenter(lonLat, zoom)
}

function addMarker(latitude, longitude, iconURL) {
    var lonLat = new OpenLayers.LonLat(longitude, latitude)
        .transform(
            new OpenLayers.Projection("EPSG:4326"),
            map.getProjectionObject()
        );

    var size = new OpenLayers.Size(21,25);
    var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
    var icon = new OpenLayers.Icon(iconURL, size, offset);
    var markers = new OpenLayers.Layer.Markers("Markers");
    map.addLayer(markers);

    markers.addMarker(new OpenLayers.Marker(lonLat, icon));
}


function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}